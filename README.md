We are specialized in museum quality art conservation and restoration of paintings, sculptures, artifacts and other artworks, from ancient to contemporary. Other services: Condition reports, disaster response, art insurance claims, preventive conservation.

Address: 1210 Roebuck Court, West Palm Beach, FL 33401, USA

Phone: 561-346-3402
